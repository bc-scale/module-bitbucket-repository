locals {
  default_branch_name = "master"
  default_branch_protections = {
    "require_approvals_to_merge" : 1,
    "push" : null,
    "require_passing_builds_to_merge" : 1,
    "delete" : null
    "require_default_reviewer_approvals_to_merge" : 1,
    "require_no_changes_requested" : null,
    "require_tasks_to_be_completed" : null,
  }

  default_protection_branch_names = [local.default_branch_name]

  branch_protections_merge      = merge(local.default_branch_protections, var.branch_protection_overrides)
  branch_protections_key_list   = setsubtract(keys(local.branch_protections_merge), var.branch_protection_ignore_rules)
  branch_protections_value_list = [for key in local.branch_protections_key_list : lookup(local.branch_protections_merge, key)]
  branch_protections            = zipmap(local.branch_protections_key_list, local.branch_protections_value_list)
  branch_protections_names      = compact(concat(local.default_protection_branch_names, var.protected_branches))

  all_branch_protections_rules_by_object = flatten([
    for rule, value in local.branch_protections : [
      for branch in local.branch_protections_names : {
        branch = branch
        rule   = rule
        value  = value
      }
    ]
  ])
  all_branch_protections_rules = {
    for element in local.all_branch_protections_rules_by_object :
    format("%s,%s", element.branch, element.rule) => element.value
  }
}

#####
# Repository
#####

resource "bitbucket_repository" "this" {
  owner       = var.owner
  name        = var.name
  slug        = var.name
  is_private  = var.is_private
  has_wiki    = var.has_wiki
  description = var.description
  project_key = var.project_key
  language    = var.language

  lifecycle {
    prevent_destroy = true
  }
}

#####
# Branching model
#####

locals {
  should_create_branching_model = var.branching_model_production != null && var.branching_model_development != null
}

resource "bitbucket_branching_model" "this" {
  for_each   = local.should_create_branching_model ? { 0 = 0 } : {}
  owner      = var.owner
  repository = bitbucket_repository.this.name

  // This has idempotency issues with provider 2.20
  dynamic "development" {
    for_each = { 0 = defaults(var.branching_model_development, {
      use_mainbranch = true
    }) }

    content {
      branch_does_not_exist = development.value.branch_does_not_exist
      use_mainbranch        = development.value.use_mainbranch
      name                  = development.value.name
    }
  }

  dynamic "production" {
    for_each = var.branching_model_production == null ? {} : { 0 = var.branching_model_production }

    content {
      enabled               = true
      branch_does_not_exist = production.value.branch_does_not_exist
      use_mainbranch        = production.value.use_mainbranch
      name                  = production.value.name
    }
  }
}

#####
# Branch protection
#####

resource "bitbucket_branch_restriction" "this" {
  for_each = local.all_branch_protections_rules

  owner      = var.owner
  repository = bitbucket_repository.this.name

  kind    = split(",", each.key)[1]
  value   = each.value
  pattern = split(",", each.key)[0]
}

#####
# Reviewers
#####

resource "bitbucket_default_reviewers" "this" {
  owner      = var.owner
  repository = bitbucket_repository.this.name

  reviewers = var.reviewers
}
