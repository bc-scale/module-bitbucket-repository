#####
# Repository
#####

variable "description" {
  type        = string
  description = "Description of the repository to create."

  validation {
    condition     = 10 <= length(var.description)
    error_message = "The var.description length must be greater than 10 characters."
  }
}

variable "has_wiki" {
  type        = bool
  description = "Whether or not the repository should have a wiki."
  default     = false
}

variable "is_private" {
  type        = bool
  description = "Whether or not the repository should be private or public. Careful, setting this to false means your repository will be at least readable for everybody in the world."
  default     = true
}

variable "name" {
  type        = string
  description = "Name of the repository to create."

  validation {
    condition     = can(regex("^[a-zA-Z0-9-]+$", var.name))
    error_message = "The var.name length must match “^[a-zA-Z0-9-]+$”."
  }
}

variable "owner" {
  type        = string
  description = "Owner of the repository to create."
}

variable "project_key" {
  type        = string
  description = "Bitbucket project key where to create the repository."

  validation {
    condition     = can(regex("^[a-zA-Z][a-zA-Z0-9_]*$", var.project_key))
    error_message = "The var.project_key length must match “^[a-zA-Z][a-zA-Z0-9_]*$”."
  }
}

variable "language" {
  type        = string
  description = "What the language of this repository should be."
  default     = null
}

#####
# Branching model
#####

variable "branching_model_development" {
  description = <<-DOCUMENTATION
    The development branch can be configured to a specific branch or to track the main branch. When set to a specific branch it must currently exist. Only the passed properties will be updated. The properties not passed will be left unchanged. A request without a development property will leave the development branch unchanged.
    * name (optional, string): The configured branch. It must be null when `use_mainbranch` is `true`. Otherwise it must be a non-empty value. It is possible for the configured branch to not exist (e.g. it was deleted after the settings are set).
    * use_mainbranch (optional, bool): Indicates if the setting points at an explicit branch (`false`) or tracks the main branch (`true`). When `true` the name must be `null` or not provided. When `false` the `name` must contain a non-empty branch name.
    * branch_does_not_exist (optional, bool): Indicates if the indicated branch exists on the repository (`false`) or not (`true`). This is useful for determining a fallback to the mainbranch when a repository is inheriting its project's branching model.
DOCUMENTATION
  type = object({
    name                  = optional(string)
    use_mainbranch        = optional(bool)
    branch_does_not_exist = optional(bool)
  })
  default = null
}

variable "branching_model_production" {
  description = <<-DOCUMENTATION
    The production branch can be configured to a specific branch or to track the main branch. When set to a specific branch it must currently exist. Only the passed properties will be updated. The properties not passed will be left unchanged. A request without a production property will leave the production branch unchanged.
    * name (optional, string): The configured branch. It must be null when `use_mainbranch` is `true`. Otherwise it must be a non-empty value. It is possible for the configured branch to not exist (e.g. it was deleted after the settings are set).
    * use_mainbranch (optional, bool): Indicates if the setting points at an explicit branch (`false`) or tracks the main branch (`true`). When `true` the name must be `null` or not provided. When `false` the `name` must contain a non-empty branch name.
    * branch_does_not_exist (optional, bool): Indicates if the indicated branch exists on the repository (`false`) or not (`true`). This is useful for determining a fallback to the mainbranch when a repository is inheriting its project's branching model.
DOCUMENTATION
  type = object({
    name                  = optional(string)
    use_mainbranch        = optional(bool)
    branch_does_not_exist = optional(bool)
  })
  default = null
}


#####
# Branch protection
#####

variable "protected_branches" {
  type        = list(string)
  default     = []
  description = "Additional branch names to protect. This exclude default branch which will always be protected."
}

variable "branch_protection_overrides" {
  type        = map(any)
  description = "Additional branch protection rules to be added to the defaults."
  default     = {}

  validation {
    condition = !contains(
      [
        for key, value in var.branch_protection_overrides :
        (
          0 == length(
            setsubtract([key], [
              "require_tasks_to_be_completed",
              "force",
              "restrict_merges",
              "enforce_merge_checks",
              "reset_pullrequest_changes_requested_on_change",
              "require_approvals_to_merge",
              "allow_auto_merge_when_builds_pass",
              "delete",
              "require_all_dependencies_merged",
              "require_no_changes_requested",
              "push",
              "require_passing_builds_to_merge",
              "reset_pullrequest_approvals_on_change",
              "require_default_reviewer_approvals_to_merge"
            ])
          )
        )
      ],
      false
    )
    error_message = "The var.branch_protection_overrides must be a set containing one of this values: 'require_tasks_to_be_completed', 'force', 'restrict_merges', 'enforce_merge_checks', 'reset_pullrequest_changes_requested_on_change', 'require_approvals_to_merge', 'allow_auto_merge_when_builds_passdelete', 'require_all_dependencies_merged', 'require_no_changes_requested', 'push', 'require_passing_builds_to_merge', 'reset_pullrequest_approvals_on_change', 'require_default_reviewer_approvals_to_merge'."
  }
}

variable "branch_protection_ignore_rules" {
  type        = list(string)
  default     = []
  description = "List of branch protection rules to ignore. Any rule in this list will be subtracted from default branch protection rules and var.branch_protection_overrides."

  validation {
    condition = !contains(
      [
        for value in var.branch_protection_ignore_rules :
        (
          0 == length(
            setsubtract([value], [
              "require_tasks_to_be_completed",
              "force",
              "restrict_merges",
              "enforce_merge_checks",
              "reset_pullrequest_changes_requested_on_change",
              "require_approvals_to_merge",
              "allow_auto_merge_when_builds_pass",
              "delete",
              "require_all_dependencies_merged",
              "require_no_changes_requested",
              "push",
              "require_passing_builds_to_merge",
              "reset_pullrequest_approvals_on_change",
              "require_default_reviewer_approvals_to_merge"
            ])
          )
        )
      ],
      false
    )
    error_message = "The var.branch_protection_ignore_rules must be a set containing one of this values: 'require_tasks_to_be_completed', 'force', 'restrict_merges', 'enforce_merge_checks', 'reset_pullrequest_changes_requested_on_change', 'require_approvals_to_merge', 'allow_auto_merge_when_builds_passdelete', 'require_all_dependencies_merged', 'require_no_changes_requested', 'push', 'require_passing_builds_to_merge', 'reset_pullrequest_approvals_on_change', 'require_default_reviewer_approvals_to_merge'."
  }
}

#####
# Reviewers
#####

variable "reviewers" {
  type        = list(string)
  description = "List bitbucket user UUIDs of people to be added to the list of reviewers for the repository to create."
  default     = []

  validation {
    condition = length(var.reviewers) == 0 || !contains(
      [
        for i in var.reviewers :
        can(regex("^{[a-f0-9\\-]+}$", i))
      ],
      false
    )
    error_message = "The var.reviewers must be a list of strings that match “^{[a-fA-Z0-9\\-]+}$”."
  }
}
