# Terraform module Bitbucket repository

Handles bitbucket repositories with Terraform.

## Usage

Intended to be used in a deployment, not directly.

```bash
$ terraform init
$ terraform plan -out /tmp/dev.out
$ terraform apply /tmp/dev.out
```


<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.14.9 |
| <a name="requirement_bitbucket"></a> [bitbucket](#requirement\_bitbucket) | ~> 2 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_bitbucket"></a> [bitbucket](#provider\_bitbucket) | ~> 2 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [bitbucket_branch_restriction.this](https://registry.terraform.io/providers/DrFaust92/bitbucket/latest/docs/resources/branch_restriction) | resource |
| [bitbucket_branching_model.this](https://registry.terraform.io/providers/DrFaust92/bitbucket/latest/docs/resources/branching_model) | resource |
| [bitbucket_default_reviewers.this](https://registry.terraform.io/providers/DrFaust92/bitbucket/latest/docs/resources/default_reviewers) | resource |
| [bitbucket_repository.this](https://registry.terraform.io/providers/DrFaust92/bitbucket/latest/docs/resources/repository) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_branch_protection_ignore_rules"></a> [branch\_protection\_ignore\_rules](#input\_branch\_protection\_ignore\_rules) | List of branch protection rules to ignore. Any rule in this list will be subtracted from default branch protection rules and var.branch\_protection\_overrides. | `list(string)` | `[]` | no |
| <a name="input_branch_protection_overrides"></a> [branch\_protection\_overrides](#input\_branch\_protection\_overrides) | Additional branch protection rules to be added to the defaults. | `map(any)` | `{}` | no |
| <a name="input_branching_model_development"></a> [branching\_model\_development](#input\_branching\_model\_development) | The development branch can be configured to a specific branch or to track the main branch. When set to a specific branch it must currently exist. Only the passed properties will be updated. The properties not passed will be left unchanged. A request without a development property will leave the development branch unchanged.<br>* name (optional, string): The configured branch. It must be null when `use_mainbranch` is `true`. Otherwise it must be a non-empty value. It is possible for the configured branch to not exist (e.g. it was deleted after the settings are set).<br>* use\_mainbranch (optional, bool): Indicates if the setting points at an explicit branch (`false`) or tracks the main branch (`true`). When `true` the name must be `null` or not provided. When `false` the `name` must contain a non-empty branch name.<br>* branch\_does\_not\_exist (optional, bool): Indicates if the indicated branch exists on the repository (`false`) or not (`true`). This is useful for determining a fallback to the mainbranch when a repository is inheriting its project's branching model. | <pre>object({<br>    name                  = optional(string)<br>    use_mainbranch        = optional(bool)<br>    branch_does_not_exist = optional(bool)<br>  })</pre> | `null` | no |
| <a name="input_branching_model_production"></a> [branching\_model\_production](#input\_branching\_model\_production) | The production branch can be configured to a specific branch or to track the main branch. When set to a specific branch it must currently exist. Only the passed properties will be updated. The properties not passed will be left unchanged. A request without a production property will leave the production branch unchanged.<br>* name (optional, string): The configured branch. It must be null when `use_mainbranch` is `true`. Otherwise it must be a non-empty value. It is possible for the configured branch to not exist (e.g. it was deleted after the settings are set).<br>* use\_mainbranch (optional, bool): Indicates if the setting points at an explicit branch (`false`) or tracks the main branch (`true`). When `true` the name must be `null` or not provided. When `false` the `name` must contain a non-empty branch name.<br>* branch\_does\_not\_exist (optional, bool): Indicates if the indicated branch exists on the repository (`false`) or not (`true`). This is useful for determining a fallback to the mainbranch when a repository is inheriting its project's branching model. | <pre>object({<br>    name                  = optional(string)<br>    use_mainbranch        = optional(bool)<br>    branch_does_not_exist = optional(bool)<br>  })</pre> | `null` | no |
| <a name="input_description"></a> [description](#input\_description) | Description of the repository to create. | `string` | n/a | yes |
| <a name="input_has_wiki"></a> [has\_wiki](#input\_has\_wiki) | Whether or not the repository should have a wiki. | `bool` | `false` | no |
| <a name="input_is_private"></a> [is\_private](#input\_is\_private) | Whether or not the repository should be private or public. Careful, setting this to false means your repository will be at least readable for everybody in the world. | `bool` | `true` | no |
| <a name="input_language"></a> [language](#input\_language) | What the language of this repository should be. | `string` | `null` | no |
| <a name="input_name"></a> [name](#input\_name) | Name of the repository to create. | `string` | n/a | yes |
| <a name="input_owner"></a> [owner](#input\_owner) | Owner of the repository to create. | `string` | n/a | yes |
| <a name="input_project_key"></a> [project\_key](#input\_project\_key) | Bitbucket project key where to create the repository. | `string` | n/a | yes |
| <a name="input_protected_branches"></a> [protected\_branches](#input\_protected\_branches) | Additional branch names to protect. This exclude default branch which will always be protected. | `list(string)` | `[]` | no |
| <a name="input_reviewers"></a> [reviewers](#input\_reviewers) | List bitbucket user UUIDs of people to be added to the list of reviewers for the repository to create. | `list(string)` | `[]` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_clone_https"></a> [clone\_https](#output\_clone\_https) | n/a |
| <a name="output_clone_ssh"></a> [clone\_ssh](#output\_clone\_ssh) | n/a |
| <a name="output_uuid"></a> [uuid](#output\_uuid) | n/a |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
