1.0.0
=====

* feat: outputs repository uuid
* feat: adds `require_no_changes_requested` by default
* feat: adds `require_tasks_to_be_completed` restriction by default
* feat: adds new options for repository:
  * `language`
* feat: adds basic branching model management with two new variables:
  * `branching_model_production`
  * `branching_model_development`
* maintenance: migrate to `DrFaust92/bitbucket` provider
* chore: pins pre-commit dependencies

0.2.0
=====

* maintenance: pins bitbucket provider to 2+
* chore: pins pre-commit dependencies

0.1.1
=====

* fix: makes sure checks on `var.branch_protection_overrides` checks key, not value

0.1.0
=====

* feat: adds is_private, has_wiki variables to control repository creation

0.0.0
=====

 * tech: initialise repository
