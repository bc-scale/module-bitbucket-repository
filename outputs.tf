#####
# Repository
#####

output "clone_https" {
  value = bitbucket_repository.this.clone_https
}

output "clone_ssh" {
  value = bitbucket_repository.this.clone_ssh
}

output "uuid" {
  value = bitbucket_repository.this.uuid
}
