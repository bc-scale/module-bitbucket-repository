terraform {
  required_version = ">= 0.14.9"

  experiments = [module_variable_optional_attrs]

  required_providers {
    bitbucket = {
      source  = "DrFaust92/bitbucket"
      version = "~> 2"
    }
  }
}
